#!make
 
.DEFAULT_GOAL := help
.PHONY: help
help:
	@echo "\033[33mUsage:\033[0m\n  make [target] [arg=\"val\"...]\n\n\033[33mTargets:\033[0m"
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' Makefile| sort | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[32m%-15s\033[0m %s\n", $$1, $$2}'

.PHONY: cache-clear
cache-clear:
	docker-compose exec api php bin/console cache:clear
	docker-compose exec api php bin/console cache:pool:clear cache.global_clearer

.PHONY: bash
bash: ## Get a shell into app container
	docker-compose exec api bash

.PHONY: logs
logs: ## Get a shell into app container
	docker-compose logs -f api

.PHONY: install
install: up ## Install project
	docker-compose exec api composer install
	docker-compose exec api bin/console doctrine:database:create --if-not-exists
	docker-compose exec api bin/console doctrine:schema:update --force

.PHONY: tests
tests: ## Run unit tests
	docker-compose exec api bin/console doctrine:schema:update --force
	docker-compose exec api bin/console doctrine:fixtures:load --no-interaction
	docker-compose exec api bin/phpunit
	
.PHONY: up
up: ## Start containers
	-docker-sync start
	docker-compose up -d

.PHONY: stop
stop: ## Stop containers
	docker-compose stop
	@docker-sync stop

.PHONY: restart
restart: stop up ## Restart containers