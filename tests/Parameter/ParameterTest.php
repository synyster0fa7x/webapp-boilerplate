<?php

namespace App\Tests\Parameter;

use App\Entity\Parameter;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Tests\Utilities\ClientUtility;

class ParameterTest extends WebTestCase
{
    use ClientUtility;

    protected function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();
    }

    public function testSetParameterForUser() 
    {
        $response = $this->request('POST', '/parameters', [
            'name' => 'locale',
            'value' => 'fr'
        ], $this->generateJWTHeader('user@ikuzo.fr', 'user'));
        $json = json_decode($response->getContent(), true);
        
        $this->assertArrayHasKey('name', $json);
        $this->assertArrayHasKey('value', $json);
        $this->assertEquals($json['value'], 'fr');
        $this->assertEquals($json['name'], 'locale');

        $id = $json['id'];

        $response = $this->request('POST', '/parameters', [
            'name' => 'locale',
            'value' => 'en'
        ], $this->generateJWTHeader('user@ikuzo.fr', 'user'));
        $json = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('name', $json);
        $this->assertArrayHasKey('value', $json);
        $this->assertEquals($json['value'], 'en');
        $this->assertEquals($json['name'], 'locale');
        $this->assertEquals($json['id'], $id);
    }

    public function testGetParameterForUser()
    {
        $response = $this->request('GET', '/parameters', null, $this->generateJWTHeader('user@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame('200');
        $json = json_decode($response->getContent(), true);
        $this->assertEquals(1, count($json['hydra:member']));
        
        $response = $this->request('GET', '/parameters', null, $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame('200');
        $json = json_decode($response->getContent(), true);
        $this->assertEquals(0, count($json['hydra:member']));
    }

    public function testParamNameValidity()
    {
        $this->request('POST', '/parameters', [
            'name' => 'locale',
            'value' => 'fr'
        ], $this->generateJWTHeader('admin@ikuzo.fr', 'user'));
        $this->assertResponseStatusCodeSame(201);

        $this->request('POST', '/parameters', [
            'name' => 'notValid',
            'value' => 'LOL'
        ], $this->generateJWTHeader('admin@ikuzo.fr', 'user'));
        $this->assertResponseStatusCodeSame(400);
    }
}