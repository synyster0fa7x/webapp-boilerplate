<?php

namespace App\Tests\Book;

use App\Entity\Book;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Tests\Utilities\ClientUtility;

class BookTest extends WebTestCase
{
    use ClientUtility;

    protected function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();
    }

    public function testCreateBook()
    {
        $this->request('POST', '/books', [
            'title' => 'title',
            'description' => 'lorem ipsum'
        ], $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(201);
    }
}