<?php

namespace App\Tests\User;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Tests\Utilities\ClientUtility;

class UserTest extends WebTestCase
{
    use ClientUtility;

    protected function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();
    }

    public function testLogin() 
    {
        $response = $this->request('POST', '/auth/login', [
            'email' => 'user@ikuzo.fr',
            'password' => 'password'
        ]);
        $json = json_decode($response->getContent(), true);
        $this->assertResponseStatusCodeSame(200);
        $this->assertArrayHasKey('token', $json);
        $this->assertArrayHasKey('refresh_token', $json);
        
        $response = $this->request('POST', '/auth/login', [
            'email' => 'user@ikuzo.fr',
            'password' => 'badPassword'
        ]);
        $this->assertResponseStatusCodeSame(401);

    }

    public function testEditUser()
    {
        $user = $this->getEntityManager()->getRepository(User::class)->findOneBy(['email' => 'user@ikuzo.fr']);
        $admin = $this->getEntityManager()->getRepository(User::class)->findOneBy(['email' => 'admin@ikuzo.fr']);

        // OK
        $response = $this->request('PUT', '/users/'.$user->getId(), [
            'firstname' => 'Johnny'
        ], $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $json = json_decode($response->getContent(), true);
        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals('Johnny', $json['firstname']);

        $response = $this->request('PUT', '/users/'.$user->getId(), [
            'firstname' => 'Johnny2'
        ], $this->generateJWTHeader('user@ikuzo.fr', 'user'));
        $json = json_decode($response->getContent(), true);
        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals('Johnny2', $json['firstname']);

        $response = $this->request('PUT', '/users/'.$admin->getId(), [
            'firstname' => 'Johnny3'
        ], $this->generateJWTHeader('user@ikuzo.fr', 'user'));
        $json = json_decode($response->getContent(), true);
        $this->assertResponseStatusCodeSame(403);
    }

    public function testCreateUser()
    {
        // Not logged user
        $this->request('POST', '/users', [
            'email' => 'notLogged@ikuzo.fr',
            'firstname' => 'James',
            'lastname' => 'Labrie',
            'plainPassword' => 'string',
        ]);
        $this->assertResponseStatusCodeSame(401);
        
        // Valid user creation
        $this->request('POST', '/users', [
            'email' => 'testCreateUser@ikuzo.fr',
            'firstname' => 'John',
            'lastname' => 'Petrucci',
            'plainPassword' => 'string',
        ], $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(201);
        
        // Not valid email assert
        $this->request('POST', '/users', [
            'email' => 'notValidEmail@ikuzofr',
            'firstname' => 'Tim',
            'lastname' => 'Henson',
            'plainPassword' => 'string',
        ], $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(400);
        
        // User's role not allowed to post new user
        $this->request('POST', '/users', [
            'email' => 'notAllowedRole@ikuzo.fr',
            'firstname' => 'Max',
            'lastname' => 'Cavalera',
            'plainPassword' => 'string',
        ], $this->generateJWTHeader('user@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(403);
        
        // Forget to set password
        $this->request('POST', '/users', [
            'email' => 'noPassword@ikuzo.fr',
            'firstname' => 'Plini',
            'lastname' => 'Roessler-Holgate'
        ], $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(500);

        // This user is already registred
        $this->request('POST', '/users', [
            'email' => 'admin@ikuzo.fr',
            'firstname' => 'Yvette',
            'lastname' => 'Young'
        ], $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(500);
        
        // An empty passeword is posted
        $this->request('POST', '/users', [
            'email' => 'emptyPassword@ikuzo.fr',
            'firstname' => 'Jason',
            'lastname' => 'Richardson',
            'plainPassword' => ''
        ], $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(500);
        
        // An user with no name
        $this->request('POST', '/users', [
            'email' => 'noName@ikuzo.fr',
            'plainPassword' => 'password'
        ], $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(500);
    }

    public function testDeleteUser()
    {
        $user = $this->getEntityManager()->getRepository(User::class)->findOneBy(['email' => 'testCreateUser@ikuzo.fr']);

        // Forbidden
        $this->request('DELETE', '/users/'.$user->getId(), null, $this->generateJWTHeader('user@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(403);
        
        // Dont exist
        $this->request('DELETE', '/users/9999999', null, $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(404);
        
        // OK, deleted
        $this->request('DELETE', '/users/'.$user->getId(), null, $this->generateJWTHeader('admin@ikuzo.fr', 'password'));
        $this->assertResponseStatusCodeSame(204);
    }
}