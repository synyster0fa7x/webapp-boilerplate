<?php

namespace App\Tests\Utilities;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;

trait ClientUtility {

    public $client;

    protected function request(string $method, string $uri, $content = null, array $headers = []): Response
    {
        $server = ['CONTENT_TYPE' => 'application/ld+json', 'HTTP_ACCEPT' => 'application/ld+json'];
        foreach ($headers as $key => $value) {
            if (strtolower($key) === 'content-type') {
                $server['CONTENT_TYPE'] = $value;

                continue;
            }

            $server['HTTP_'.strtoupper(str_replace('-', '_', $key))] = $value;
        }

        if (is_array($content) && false !== preg_match('#^application/(?:.+\+)?json$#', $server['CONTENT_TYPE'])) {
            $content = json_encode($content);
        }

        $this->client->request($method, $uri, [], [], $server, $content);

        return $this->client->getResponse();
    }

    protected function generateToken(string $email, string $password) : string
    {
        return $this->client->getContainer()
            ->get('lexik_jwt_authentication.encoder')
            ->encode(['email' => $email, 'password' => $password]);
    }

    protected function generateJWTHeader(string $email, string $password) : array
    {
        return ['Authorization' => 'Bearer '.$this->generateToken($email, $password)];
    }

    protected function getEntityManager() : ObjectManager
    {
        return $this->client->getContainer()->get('doctrine.orm.entity_manager');
    }
}