<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ParameterName extends Constraint
{
    public $message = 'The parameter name "{{ value }}" is not valid.';
}
