<?php

namespace App\Validator;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ParameterNameValidator extends ConstraintValidator
{
    private $params;

    public function __construct(ContainerBagInterface $params)
    {
        $this->params = $params;
    }

    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\ParameterName */

        if (null === $value || '' === $value) {
            return;
        }

        if (in_array($value, $this->params->get('parameter_entity_names'))) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $value)
            ->addViolation();
    }
}
