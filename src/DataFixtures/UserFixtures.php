<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $users = [
            [
                'email' => 'user@ikuzo.fr',
                'firstname' => 'John',
                'lastname' => 'Doe',
                'password' => 'password',
                'roles' => ['ROLE_USER'],
            ],
            [
                'email' => 'admin@ikuzo.fr',
                'firstname' => 'Rose',
                'lastname' => 'Marry',
                'password' => 'password',
                'roles' => ['ROLE_ADMIN']
            ],
        ];

        foreach ($users as $line) {
            $user = new User();

            $password = $this->encoder->encodePassword($user, $line['password']);

            $user
                ->setEmail($line['email'])
                ->setFirstname($line['firstname'])
                ->setLastname($line['lastname'])
                ->setPassword($password)
                ->setRoles($line['roles']);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
