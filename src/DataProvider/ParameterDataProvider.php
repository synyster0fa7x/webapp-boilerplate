<?php
// api/src/DataProvider/BlogPostCollectionDataProvider.php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Parameter;
use Symfony\Component\Security\Core\Security;

final class ParameterDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface, ItemDataProviderInterface
{
    private $em;
    private $security;
    
    public function __construct(EntityManagerInterface $em, Security $security )
    {
        $this->em = $em;
        $this->security = $security;
    }
    
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Parameter::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null): \Generator
    {
        $rows = $this->em->getRepository(Parameter::class)->findBy(['user' => $this->security->getUser()]);
        
        if ($rows) {
            yield $rows;
        } else {
            return [];
        }
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Parameter
    {
        $row = $this->em->getRepository(Parameter::class)->findOneBy([
            'id' => $id, 
            'user' => $this->security->getUser()]
        );
        
        return $row;
    }
}