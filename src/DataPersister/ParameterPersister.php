<?php

namespace App\DataPersister;

use App\Entity\User;
use App\Entity\Parameter;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class ParameterPersister implements DataPersisterInterface
{
    private $em;
    private $security;
    
    public function __construct(EntityManagerInterface $em, Security $security )
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function supports($data): bool
    {
        return $data instanceof Parameter;
    }

    /**
     * @param Parameter $data
     */
    public function persist($data)
    {
        $data->setUser($this->security->getUser());

        $row = $this->em->getRepository(Parameter::class)->findOneBy([
            'name' => $data->getName(),
            'user' => $data->getUser()
        ]);

        if ($row) {
            $row->setValue($data->getValue());
            $data = $row;
        } else {
            $this->em->persist($data);
        }
        
        $this->em->flush();

        return $data;
    }
    
    /**
     * @param User $data
     */
    public function remove($data)
    {
        $this->em->remove($data);
        $this->em->flush();
    }
}